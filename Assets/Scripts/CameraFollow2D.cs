﻿using Unavia.Utilities;
using UnityEngine;

public class CameraFollow2D : MonoBehaviour
{
    #region Fields
    [SerializeField]
    [Range(0f, 10f)]
    private float smoothSpeed = 5f;
    [SerializeField]
    [Range(0f, 5f)]
    private float maxOffset = 3f;
    [Header("Game Objects")]
    [SerializeField]
    private Transform target = null;
    #endregion

    // Z position never changes
    private float zPosition;


    #region Unity Methods
    private void Start()
    {
        zPosition = transform.position.z;
    }

    // NOTE: Use LateUpdate as recommended by Unity for follow cameras
    void LateUpdate()
    {
        if (target == null) return;

        Vector3 desiredPosition = new Vector3(target.position.x, target.position.y, zPosition);
        Vector3 smoothedPosition = Vector3.Lerp(transform.position, desiredPosition, smoothSpeed * Time.deltaTime);

        // Camera should remain within specified offset of target (even with smoothing)
        if (Vector3.Distance(smoothedPosition, desiredPosition) > maxOffset)
        {
            smoothedPosition = Vector3Extensions.PointAlongLine(desiredPosition, smoothedPosition, maxOffset);
        }

        transform.position = smoothedPosition;
    }
    #endregion
}

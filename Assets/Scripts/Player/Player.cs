﻿using Drawing;
using Sirenix.OdinInspector;
using System.Collections;
using TMPro;
using Unavia.Utilities;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Rigidbody2D))]
public class Player : MonoBehaviour
{
    #region Fields
    [SerializeField]
    [Range(0f, 360f)]
    [Tooltip("Degrees per second")]
    private float weaponSpeed = 120f;
    [SerializeField]
    [Range(0f, 1f)]
    [Tooltip("How long the weapon takes to fill")]
    private float weaponChargeTime = 0.5f;
    [SerializeField]
    [Range(0f, 25f)]
    private float maxWeaponKnockback = 15f;
    /// <summary>
    /// Time to fully change weapon movement (ie. stopping/starting)
    /// </summary>
    [SerializeField]
    [Range(0f, 0.5f)]
    [Tooltip("Time to fully change weapon movement direction")]
    private float weaponLerpTime = 0.1f;
    [SerializeField]
    [Range(0f, 0.5f)]
    private float timeBetweenShots = 0.05f;
    [SerializeField]
    private bool weaponCanReverse = true;
    /// <summary>
    /// Whether player can move as a result of firing
    /// </summary>
    [SerializeField]
    private bool playerCanMove = true;

    [FoldoutGroup("Colors", Expanded = true)]
    [SerializeField]
    private Color playerColor = Color.yellow;
    [FoldoutGroup("Colors")]
    [SerializeField]
    private Color playerOverlayColor = Color.yellow;
    [FoldoutGroup("Colors")]
    [SerializeField]
    private Color weaponEmptyColor = Color.grey;
    [FoldoutGroup("Colors")]
    [SerializeField]
    private Color weaponChargingColor = Color.yellow;
    [FoldoutGroup("Colors")]
    [ColorUsage(true, true)]
    [SerializeField]
    private Color projectileColor = Color.white;
    [FoldoutGroup("Colors")]
    [Button("Apply Colors")]
    private void OnApplyColors() { ApplyColors(); }

    [Header("Game Objects")]
    [SerializeField]
    private SpriteRenderer playerSprite = default;
    [SerializeField]
    private SpriteRenderer playerOverlaySprite = default;
    [SerializeField]
    private SpriteRenderer playerShadowSprite = default;
    [SerializeField]
    private SpriteRenderer weaponSprite = default;
    [SerializeField]
    private GameObject weaponObject = default;
    [SerializeField]
    private ParticleSystem weaponParticles = default;

    [Header("Debug")]
    [SerializeField]
    private bool showForceDirection = false;
    [SerializeField]
    private Slider weaponChargeSlider = default;
    [SerializeField]
    private Slider weaponSpeedSlider = default;
    #endregion

    private Vector3 forceDirection => (transform.position - weaponObject.transform.position).normalized;
    private float chargePercent => !isMoving ? timeInCurrentState / weaponChargeTime : 0f;
    private float forceMagnitude => maxWeaponKnockback * chargePercent;
    private Vector3 force => forceMagnitude * forceDirection;

    private Rigidbody2D rb;
    private Actions actions;
    private Vector3 weaponRotationAxis = new Vector3(0f, 0f, 1f);

    private bool isMoving = true;
    private float timeInCurrentState = 0f;
    private float currentWeaponSpeed;
    private Coroutine changeWeaponSpeedCoroutine;
    private Projectile currentProjectile;


    #region Unity Methods
    void Awake()
    {
        isMoving = true;
        currentWeaponSpeed = weaponSpeed;
        timeInCurrentState = 0f;
        currentProjectile = null;

        ApplyColors();
        playerShadowSprite.gameObject.SetActive(true);

        // NOTE: Don't need to use PlayerInput because there is only one player
        actions = new Actions();
        actions.Player.Shoot.performed += ctx => ChangeMoveState(true);
        actions.Player.Shoot.canceled += ctx => ChangeMoveState(false);
        actions.Player.Reverse.performed += ctx => ReverseRotation();

        rb = GetComponent<Rigidbody2D>();
    }

    private void OnEnable()
    {
        actions.Enable();
    }

    private void OnDisable()
    {
        actions.Disable();
    }

    void Update()
    {
        timeInCurrentState += Time.deltaTime;

        weaponChargeSlider.value = chargePercent;
        weaponSpeedSlider.value = currentWeaponSpeed.MapToPercent(0f, weaponSpeed);

        // Move the weapon around the player
        UpdateWeaponMovement();

        // Update the projectile while charging
        if (currentProjectile != null)
        {
            currentProjectile.OnCharge(chargePercent, weaponObject.transform.position);
        }

        CommandBuilder debugDraw = showForceDirection ? Draw.ingame : Draw.editor;
        // Indicate the weapon force direction
        debugDraw.Arrowhead(weaponObject.transform.position - forceDirection, forceDirection, Vector3.forward, 0.3f, Color.white);
    }

    private void OnDestroy()
    {
        actions.Player.Shoot.performed -= ctx => ChangeMoveState(true);
        actions.Player.Shoot.canceled -= ctx => ChangeMoveState(false);
        actions.Player.Reverse.performed -= ctx => ReverseRotation();
    }
    #endregion


    #region Custom Methods
    /// <summary>
    /// Update the weapon movement
    /// </summary>
    private void UpdateWeaponMovement()
    {
        if (currentWeaponSpeed > 0f)
        {
            weaponObject.transform.RotateAround(transform.position, weaponRotationAxis, currentWeaponSpeed * Time.deltaTime);
        }

        // Shot should automatically fire when weapon is full
        if (timeInCurrentState >= weaponChargeTime)
        {
            ChangeMoveState(false);
        }
    }

    private void ApplyColors()
    {
        playerSprite.color = playerColor;
        playerOverlaySprite.color = playerOverlayColor;
        weaponSprite.color = !isMoving ? weaponChargingColor : weaponEmptyColor;
    }

    /// <summary>
    /// Handle changes to the move state (from user input)
    /// </summary>
    /// <param name="wasPressed">Whether the button was pressed or released</param>
    private void ChangeMoveState(bool wasPressed)
    {
        bool wasReleased = !wasPressed;
        // Prevent shooting while moving (ie. shooting again after auto-shot).
        //   This should only happen when the player holds the key past the auto-shot,
        //   which would normally call this method again on release.
        if (wasReleased && isMoving) return;
        // Prevent shooting too quickly (couples well with above check).
        if (wasPressed && timeInCurrentState < timeBetweenShots) return;

        if (wasReleased)
        {
            if (playerCanMove)
            {
                rb.AddForce(force, ForceMode2D.Impulse);
            }

            // Projectiles need to move away from the weapon
            Fire(-forceDirection);

            currentProjectile = null;
        }
        else
        {
            // TODO: Figure out how to properly apply the initial rotation
            GameObject projectileObject = PoolManager.Instance.SpawnFromPool(
                "Projectiles",
                weaponObject.transform.position,
                Quaternion.identity
            );
            currentProjectile = projectileObject.GetComponent<Projectile>();
            currentProjectile.OnSpawnInit(projectileColor);
        }

        weaponSprite.color = wasPressed ? weaponChargingColor : weaponEmptyColor;
        playerShadowSprite.gameObject.SetActive(wasReleased);

        // Any action should always reset the stopped timer and movement state
        timeInCurrentState = 0f;
        isMoving = wasReleased;

        // Smoothly lerp between weapon movement states
        // NOTE: This must happen AFTER setting "timeInCurrentState"...
        if (changeWeaponSpeedCoroutine != null)
        {
            StopCoroutine(changeWeaponSpeedCoroutine);
        }
        changeWeaponSpeedCoroutine = StartCoroutine(LerpWeaponSpeed(wasPressed));
    }

    /// <summary>
    /// Smoothly lerp between weapon movement states
    /// </summary>
    /// <param name="isStopping">Whether weapon is stopping</param>
    /// <returns>Coroutine</returns>
    private IEnumerator LerpWeaponSpeed(bool isStopping)
    {
        // The lerp should only happen between the target and current speeds,
        //    over the amount of time this difference requires.
        float startingWeaponSpeed = currentWeaponSpeed;
        float lerpTime = isStopping
            ? currentWeaponSpeed.Map(0f, weaponSpeed, 0f, weaponLerpTime)
            : currentWeaponSpeed.Map(0f, weaponSpeed, weaponLerpTime, 0f);

        while (timeInCurrentState <= lerpTime)
        {
            float targetSpeed = isStopping ? 0f : weaponSpeed;
            currentWeaponSpeed = timeInCurrentState.Map(0f, lerpTime, startingWeaponSpeed, targetSpeed);
            currentWeaponSpeed = currentWeaponSpeed.Clamp(0f, weaponSpeed);

            yield return null;
        }

        // Ensure that weapon speed always ends with a correct value (only called if coroutine finishes normally)
        currentWeaponSpeed = isStopping ? 0f : weaponSpeed;
    }

    /// <summary>
    /// Reverse the weapon rotation
    /// </summary>
    private void ReverseRotation()
    {
        // Prevent reversing rotation while charging a shot (unexpected behaviour)
        if (!isMoving || !weaponCanReverse) return;

        weaponRotationAxis = -weaponRotationAxis;

        // TODO: Use "weaponLerpTime" to slow down and reverse direction...
    }

    private void Fire(Vector2 forceDirection)
    {
        currentProjectile.Shoot(chargePercent, forceDirection);
        currentProjectile = null;

        weaponParticles.Play();
    }
    #endregion
}

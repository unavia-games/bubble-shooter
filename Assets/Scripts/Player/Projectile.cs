﻿using Drawing;
using Sirenix.OdinInspector;
using System.Collections;
using Unavia.Utilities;
using UnityEngine;
using UnityEngine.Experimental.Rendering.Universal;

[RequireComponent(typeof(Rigidbody2D))]
public class Projectile : MonoBehaviour, IPoolObject
{
    #region Fields
    [SerializeField]
    [Tooltip("Force range (based on charge)")]
    [MinMaxSlider(0f, 10f, true)]
    private Vector2 forceRange = new Vector2(3f, 6f);
    [SerializeField]
    [Tooltip("Damage range (based on charge)")]
    [MinMaxSlider(5f, 50f, true)]
    private Vector2 damageRange = new Vector2(5f, 50f);
    [SerializeField]
    [Tooltip("Size range (based on charge)")]
    [MinMaxSlider(0f, 1f, true)]
    private Vector2 sizeRange = new Vector2(0.25f, 1f);
    [Tooltip("Light intensity range (based on charge")]
    [MinMaxSlider(0f, 1f, true)]
    private Vector2 lightIntensityRange = new Vector2(0.3f, 1f);

    [SerializeField]
    [Range(1f, 30f)]
    private float maxLifetime = 10f;

    [Header("Game Objects")]
    [SerializeField]
    private new Light2D light = default;
    #endregion

    public GameObject GameObject => gameObject;

    private Rigidbody2D rb;
    private Material material;
    private Coroutine autoDeath;

    private float damage;


    #region Unity Methods
    private void OnCollisionEnter2D(Collision2D collision)
    {
        // TODO
    }
    #endregion


    #region Custom Methods
    public void OnCreate()
    {
        rb = GetComponent<Rigidbody2D>();
        material = GetComponent<Renderer>().material;
    }

    public void OnSpawn()
    {
        // Ensure velocity is reset when spawning from pool
        rb.velocity = Vector2.zero;
    }

    /// <summary>
    /// Initialize the projectile (after claiming from the pool)
    /// </summary>
    /// <param name="color">Color</param>
    public void OnSpawnInit(Color color)
    {
        material.SetColor("_Color", color);

        // Projectiles start with no charge
        OnCharge(0f, transform.position);
    }

    /// <summary>
    /// Update the projectile while charging
    /// </summary>
    /// <param name="chargePercent">Percent charged</param>
    public void OnCharge(float chargePercent, Vector3 position)
    {
        float size = chargePercent.MapFromPercent(sizeRange.x, sizeRange.y);
        transform.localScale = new Vector3(size, size, size);
        transform.position = position;
        light.intensity = chargePercent.MapFromPercent(lightIntensityRange.x, lightIntensityRange.y);
    }

    /// <summary>
    /// Shoot the projectile
    /// </summary>
    /// <param name="chargePercent">Projectile charge percent</param>
    /// <param name="forceDirection">Force direction</param>
    public void Shoot(float chargePercent, Vector2 forceDirection)
    {
        // Damage, size, and force are all based on weapon charge
        damage = chargePercent.MapFromPercent(damageRange.x, damageRange.y);

        float forceMagnitude = chargePercent.MapFromPercent(forceRange.x, forceRange.y);
        Vector3 force = forceMagnitude * forceDirection;

        rb.AddForce(force, ForceMode2D.Impulse);

        // TODO: Fix bug as it can sometimes be inactive...
        autoDeath = StartCoroutine(AutoDeath(maxLifetime));
    }

    public void OnReclaim()
    {
        gameObject.SetActive(false);

        rb.velocity = Vector2.zero;

        // Ensure autodeath coroutine is stopped
        StopCoroutine(autoDeath);
    }

    /// <summary>
    /// Projectiles should automatically die after a set time
    /// </summary>
    /// <param name="lifetime">Maximum lifetime</param>
    /// <returns>Coroutine</returns>
    private IEnumerator AutoDeath(float lifetime)
    {
        yield return new WaitForSeconds(lifetime);

        OnReclaim();
    }
    #endregion
}
